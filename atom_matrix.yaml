substitutions:
  devicename: gutenberg
  upper_devicename: Gutenberg
  matrixwidth: "5"
  matrixheight: "5"
  sensora: sht3xd
  sensorb: bmp280
  update: 30s

globals:
  - id: oldT
    type: float
    initial_value: '0.0f'
  - id: oldp
    type: float
    initial_value: '0.0f'
  - id: oldh
    type: float
    initial_value: '0.0f'
  - id: t
    type: int
    initial_value: '0'
  - id: it
    type: int
    initial_value: '30'
  - id: ft
    type: int
    initial_value: '600'
  - id: display_on
    type: bool
    initial_value: 'true'

esphome:
  name: $devicename
  platform: ESP32
  board: pico32
  on_boot:
    priority: -10
    then: # stop the flickering
      - lambda: |-
            fastled_base_fastledlightoutput->get_controller()->setDither(0);
      - light.turn_on:
            id: matrix
            brightness: 20%

wifi:
  ssid: !secret wifi
  password: !secret wifi_password

  ap:
    ssid: ${devicename} Fallback Hotspot
    password: !secret ota_password

captive_portal:

logger:

ota:
  password: !secret ota_password

mqtt:
  broker: !secret mqtt_broker
  username: !secret mqtt_user
  password: !secret mqtt_password
  discovery: false

text_sensor:
  - platform: mqtt_subscribe
    name: ${upper_devicename} Text
    id: text_m
    topic: atom/${devicename}/text
  - platform: wifi_info
    ip_address:
      id: my_ip
      name: "ip"
      internal: true

font:
  - id: tinyfont
    file: "fonts/DejaVuSans.ttf"
    size: 7

binary_sensor:
  - platform: gpio
    id: b1
    pin:
      number: GPIO39
      inverted: true
    name: ${upper_devicename} Button
    on_press:
      then:
        - lambda: |-
            id(display_on) =  !id(display_on);

switch:
  - platform: restart
    name: ${upper_devicename} Restart

light:
  - platform: fastled_clockless
    chipset: WS2812B
    id: matrix
    pin: GPIO27
    num_leds: 25
    rgb_order: GRB
    name: ${upper_devicename} Led Matrix
    color_correct: [20%, 20%, 20%]
    restore_mode: ALWAYS_ON


display:
   - platform: addressable_light
     id: matrix_display
     addressable_light_id: matrix
     width: 5
     height: 5
     rotation: 0°
     update_interval: 300ms
     lambda: |-
       
       if (id(display_on) ){
         static uint16_t x = 0;
         int xs, ys;
         int width, height;
         const char * text = id(text_m).state.c_str();
       
         Color c;
         if(text[0] == '+')
           c = Color(0, 255, 0);
         else if(text[0] == '-')
           c = Color(255, 0, 0);
         else
           c = Color( 0, 0, 255);

         it.get_text_bounds(0, 0, text, id(tinyfont), TextAlign::LEFT, &xs, &ys, &width, &height);

         it.print(-(x % (width + 5)), -2 ,id(tinyfont), c,TextAlign::LEFT, text);
         x++;
       }
i2c:
  - id: bus_a
    sda: GPIO26
    scl: GPIO32
    scan: True
  - id: bus_b
    sda: GPIO25
    scl: GPIO21
    scan: True

script:
  - id: my_publish
    then:
      - mqtt.publish_json:
          retain: true
          qos: 0
          topic: sensor/atom/${devicename}
          payload: |-
            root["location"] = "${devicename}";
            root["temperature"] = id(my_temp).state;
            root["pressure"] = id(my_pres).state;
            root["humidity"] = id(my_hum).state;
            root["rssi"] = id(my_rssi).state;
            root["ip"] = id(my_ip).state;
            root["ax"] = id(my_ax).state;
            root["ay"] = id(my_ay).state;
            root["az"] = id(my_az).state;
            root["gx"] = id(my_gx).state;
            root["gy"] = id(my_gy).state;
            root["gz"] = id(my_gz).state;
            root["gt"] = id(my_gt).state;
      - mqtt.publish:
          retain: true
          qos: 0
          topic: atom/${devicename}/text
          payload: !lambda |-
            static char T[32];
            sprintf(T, "T:%5.2f°C p:%7.2fhPa h:%5.2f%%", id(my_temp).state, id(my_pres).state,  id(my_hum).state);
            return to_string(T);
sensor:
  - platform: wifi_signal
    name: "wifi signal"
    id: my_rssi
    internal: true
    update_interval: ${update}

  - platform: $sensora
    id: my_sht30T
    address: 0x44
    i2c_id: bus_a
    update_interval: ${update}
    temperature:
      id: my_temp
      name: ${sensora} ${devicename} Temperature
      accuracy_decimals: 2
      internal: true
    humidity:
      id: my_hum
      name: ${sensora} ${devicename} Humidity
      accuracy_decimals: 2
      internal: true

  - platform: $sensorb
    id: my_bmp280
    i2c_id: bus_a
    address: 0x76
    update_interval: ${update}
    pressure:
      id: my_pres
      name: ${sensorb} ${devicename} Pressure
      accuracy_decimals: 2
      internal: true
      filters:
        - offset: -3.0
  - platform: mpu6886
    i2c_id: bus_b
    id: my_gyro
    address: 0x68
    accel_x:
      name: "MPU6886 Accel X"
      id: my_ax
    accel_y:
      name: "MPU6886 Accel Y"
      id: my_ay
    accel_z:
      name: "MPU6886 Accel z"
      id: my_az
    gyro_x:
      name: "MPU6886 Gyro X"
      id: my_gx
    gyro_y:
      name: "MPU6886 Gyro Y"
      id: my_gy
    gyro_z:
      name: "MPU6886 Gyro z"
      id: my_gz
    temperature:
      id: my_gt
      name: "MPU6886 Temperature"

remote_receiver:
  pin:
    number: GPIO32
    inverted: true
  dump: all

interval:
  - interval: ${update}
    then:
      - if:
          condition:
            lambda: |-
              id(t) += id(it);
              float T = id(my_temp).state;
              float p = id(my_pres).state;
              float h = id(my_hum).state;
              if ( (abs(T - id(oldT)) > 0.1f) || (abs(p - id(oldp)) > 0.5f) || (abs(h-id(oldh) > 0.1f)) ){
                id(oldT) = T;
                id(oldp) = p;
                id(oldh) = h;
                return true;
              } else {
                if( (id(t)%id(ft) == 0) && (id(t) != 0) ) {
                  id(t) = 0;
                  return true;
                } else {
                  return false;
                }
              }
          then:
            - script.execute: my_publish

