substitutions:
  devicename: uranus
  sensor: bme280
  sensor_airq2: sgp4x
  update: 30s

esphome:
  name: $devicename
  platformio_options:
    board_build.flash_mode: dio

globals:
  - id: oldT
    type: float
    initial_value: '0.0f'
  - id: oldp
    type: float
    initial_value: '0.0f'
  - id: oldh
    type: float
    initial_value: '0.0f'
  - id: oldvoc
    type: float
    initial_value: '0.0f'
  - id: t_garden
    type: float
    initial_value: '0.0f'
  - id: h_garden
    type: float
    initial_value: '0.0f'
  - id: t
    type: int
    initial_value: '0'
  - id: it
    type: int
    initial_value: '30'
  - id: ft
    type: int
    initial_value: '600'

  - id: keys
    type: std::vector<std::string>
    restore_value: no
    initial_value: '{"0","1","2","3"}'
  
  - id: Ts
    type: std::vector<int>
    restore_value: no
    initial_value: '{0,0,0,0,0,0,0,0}'
  - id: pp
    type: std::vector<int>
    restore_value: no
    initial_value: '{0,0,0,0,0,0,0,0}'
  - id: wind
    type: std::vector<float>
    restore_value: no
    initial_value: '{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}'

  - id: days
    type: std::vector<std::string>
    restore_value: no
    initial_value: '{"Xxx","Yyy","Zzz","Ttt"}'
  
  - id: xp
    type: std::vector<int>
    restore_value: no
    initial_value: '{45,90,135,180}'
  
  - id: code
    type: std::vector<int>
    restore_value: no
    initial_value: '{0,0,0,0}'
  - id: icon
    type: std::vector< const char * >
    restore_value: no
    initial_value: '{"\U000F0594", "\U000F0599", "\U000F0F31", "\U000F0595", "\U000F0F33", "\U000F0591", "\U000F0590", "\U000F0590", "\U000F0F33", "\U000F0F33", "\U000F0597", "\U000F0597", "\U000F0596", "\U000F0596", "\U000F0596", "\U000F067F", "\U000F067F", "\U000F067F", "\U000F0592", "\U000F0592", "\U000F0592", "\U000F0598", "\U000F0598", "\U000F0598", "\U000F0F36", "\U000F0F36", "\U000F0F36", "\U000F067E", "\U000F067E", "\U000F0593"}'


esp32:
  board: esp32-c3-devkitm-1
  framework:
    type: esp-idf
    version: recommended
    # Custom sdkconfig options
    sdkconfig_options:
      CONFIG_COMPILER_OPTIMIZATION_SIZE: y
    # Advanced tweaking options
    advanced:
      ignore_efuse_mac_crc: false

wifi:
  ssid: !secret wifi
  password: !secret wifi_password

  # Enable fallback hotspot (captive portal) in case wifi connection fails
  ap:
    ssid: ${devicename} Fallback Hotspot
    password: "MLO4IA0MZb10"

# captive_portal:

# Enable logging
logger:
#  level: VERY_VERBOSE
# Enable Home Assistant API
# api:
#  password: !secret ota_password

ota:
  password: !secret ota_password

mqtt:
  broker: !secret mqtt_broker
#  username: !secret mqtt_user
#  password: !secret mqtt_password
  discovery: false
  on_json_message:
    - topic: sensor/bme280/garden
      then:
        -  lambda: |-
            if (x.containsKey("temperature")){
              id(t_garden) = x["temperature"];
            }
            if (x.containsKey("humidity")){
              id(h_garden) = x["humidity"];
            }

    - topic: sensor/weather/forecast
      then:
        -  lambda: |-
            int i = 0;
            int n = id(keys).size();
            for (auto k: id(keys)){
              if (x.containsKey(k)){
                id(days)[i] = x[k]["day"].as<const char *>();
                id(Ts)[i] = x[k]["nT"];
                id(Ts)[i+n] = x[k]["dT"];
                id(pp)[i] = x[k]["nPP"];
                id(pp)[i+n] = x[k]["dPP"];
                id(wind)[i] = atof(x[k]["nwind_speed"].as<const char *>());
                id(wind)[i+n] = atof(x[k]["dwind_speed"].as<const char *>());
                id(code)[i] = atoi(x[k]["code"].as<const char *>());
                i++;
                }
             }

i2c:
  - id: bus_b
    sda: GPIO08
    scl: GPIO09

script:
  - id: my_publish
    then:
      - mqtt.publish_json:
          retain: true
          qos: 0
          topic: sensor/${sensor}/${devicename}
          payload: |-
            root["location"] = "${devicename}";
            root["temperature"] = id(my_temp).state;
            root["pressure"] = id(my_pres).state;
            root["humidity"] = id(my_hum).state;
            root["rssi"] = id(my_rssi).state;
            root["ip"] = id(my_ip).state;
            root["voc"] = id(my_voc).state;

text_sensor:
  - platform: wifi_info
    ip_address:
      id: my_ip
      name: "ip"
      internal: true
  - platform: mqtt_subscribe
    name: "garden"
    id: garden
    topic: sensor/bme280/garden
    internal: true
  - platform: mqtt_subscribe
    name: "forecast"
    id: forecast
    topic: sensor/weather/forecast
    internal: true

sensor:
  - platform: wifi_signal
    name: "wifi signal"
    id: my_rssi
    internal: true
    update_interval: ${update}
  - platform: ${sensor}_i2c
    id: my_bme
    address: 0x76
    i2c_id: bus_b
    update_interval: ${update}
    temperature:
      id: my_temp
      name: ${sensor} ${devicename} Temperature
      oversampling: 16x
      accuracy_decimals: 2
      internal: true
    pressure:
      name: ${sensor} ${devicename} Pressure
      id: my_pres
      accuracy_decimals: 2
      internal: true
      filters:
        - offset: -3.0
    humidity:
      name: ${sensor} ${devicename} Humidity
      id: my_hum
      internal: true
      accuracy_decimals: 2
  - platform: ${sensor_airq2}
    address: 0x59
    i2c_id: bus_b
    update_interval: ${update}
    voc:
      name: "${sensor_airq2} ${devicename} VOC Index"
      id: my_voc
      accuracy_decimals: 1
    compensation:
      temperature_source: my_temp
      humidity_source: my_hum
spi:
  id: bus_a
  clk_pin: GPIO04
  mosi_pin: GPIO06
  miso_pin: GPIO05

<<: !include fonts/fonts.yaml

time:
  - platform: sntp
    timezone: 'Europe/London'
    id: my_time

display:
  - platform: waveshare_epaper
    spi_id: bus_a
    model: "1.54inv2"
    reset_pin: GPIO02
    cs_pin: GPIO07
    dc_pin: GPIO01
    busy_pin: GPIO3
    rotation: 90
    full_update_every: 30
    update_interval: 30s
    lambda: |-
      float T = id(my_temp).state;
      float Tg = id(t_garden);
      float p = id(my_pres).state;
      float h = id(my_hum).state;
      float hg = id(h_garden);
      float v = id(my_voc).state;

      it.printf(0, 5, id(my_font),TextAlign::LEFT,"%s", "uranus");
      it.line(0,18,200,18);
      it.printf(80, 120, id(my_font), TextAlign::RIGHT , "%3.1f/%3.1f°C", Tg, T);
      it.print(90, 120,  id(mdi_small), TextAlign::CENTER_HORIZONTAL, "\U000F050F"); //thermometer

      it.printf(80, 140, id(my_font), TextAlign::RIGHT , "%2.0f/%2.0f%%", hg, h);
      it.print(90, 140, id(mdi_small), TextAlign::CENTER_HORIZONTAL, "\U000F1A36"); //w

      it.printf(170, 120, id(my_font), TextAlign::RIGHT , "%4.0f hPa", p);
      it.print(190, 120, id(mdi_small), TextAlign::CENTER_HORIZONTAL, "\U000F029A"); //gauge

      it.printf(170, 140, id(my_font), TextAlign::RIGHT , "%3.0f ppb", v);
      it.print(190, 140, id(mdi_small), TextAlign::CENTER_HORIZONTAL, "\U000F0D43"); //air

      float r = id(my_rssi).state;
      it.printf(0, 188, id(my_font_wifi), TextAlign::LEFT, "\U000F091F");
      if (r < -75.0f){
        it.printf(0, 188, id(my_font_wifi), TextAlign::LEFT, "\U000F091F");
      } else if (r < -50.0f) {
        it.printf(0, 188, id(my_font_wifi), TextAlign::LEFT, "\U000F0922");
      } else if (r < -25.0f) {
        it.printf(0, 188, id(my_font_wifi), TextAlign::LEFT, "\U000F0925");
      } else{
        it.printf(0, 188, id(my_font_wifi), TextAlign::LEFT, "\U000F0928");
      }
      it.line(0,188,200,188);
      it.printf(12, 188, id(my_font), " %s",id(my_ip).state.c_str());
      it.strftime(0,160 , id(my_font_clock), "%H:%M", id(my_time).now());
      it.strftime(80, 160, id(my_font), "%a, %d-%b-%Y", id(my_time).now());

      /*
      it.print(45, 22, id(mdi_weather), TextAlign::CENTER_HORIZONTAL, "󱮕"); //mdi-weather-cloudy
      it.print(90, 22, id(mdi_weather), TextAlign::CENTER_HORIZONTAL, "\U000F0596"); // mdi-weather-sunny
      it.print(135, 22, id(mdi_weather), TextAlign::CENTER_HORIZONTAL, "󱋋"); // mdi-weather-sunny
      it.print(180, 22, id(mdi_weather), TextAlign::CENTER_HORIZONTAL, "󰼱"); // mdi-weather-sunny
      */
      it.print(10, 43,  id(mdi_small), TextAlign::CENTER_HORIZONTAL, "\U000F050F"); 
      it.print(10, 59,  id(mdi_small), TextAlign::CENTER_HORIZONTAL, "\U000F1A36"); 
      it.print( 10, 77,  id(mdi_small), TextAlign::CENTER_HORIZONTAL, "\U000F059D"); 
      it.print( 10,  87,  id(my_font_small), TextAlign::CENTER_HORIZONTAL, "m/s"); 
      
      ESP_LOGD("custom", "This is a custom debug message");
      int n = id(keys).size();
      std::vector< const char *> xx {"\U000F0594", "\U000F0599", "\U000F0F31", "\U000F0595", "\U000F0F33", "\U000F0591", "\U000F0590", "\U000F0590", "\U000F0F33", "\U000F0F33", "\U000F0597", "\U000F0597", "\U000F0596", "\U000F0596", "\U000F0596", "\U000F067F", "\U000F067F", "\U000F067F", "\U000F0592", "\U000F0592", "\U000F0592", "\U000F0598", "\U000F0598", "\U000F0598", "\U000F0F36", "\U000F0F36", "\U000F0F36", "\U000F067E", "\U000F067E", "\U000F0593"};
      for( int i=0;i<n;++i){  
        it.print(id(xp)[i], 22, id(mdi_weather), TextAlign::CENTER_HORIZONTAL, id(icon)[id(code)[i]]); 
        it.printf(id(xp)[i], 48,id(my_font_small), TextAlign::CENTER_HORIZONTAL, "%3d/%3d",id(Ts)[i],id(Ts)[i+n]); 
        it.printf(id(xp)[i], 61,id(my_font_small), TextAlign::CENTER_HORIZONTAL, "%2d/%2d",id(pp)[i],id(pp)[i+n]); 
        it.printf(id(xp)[i], 73,id(my_font_small), TextAlign::CENTER_HORIZONTAL, "%3.1f",id(wind)[i]); 
        it.printf(id(xp)[i], 85,id(my_font_small), TextAlign::CENTER_HORIZONTAL, "%3.1f",id(wind)[i+n]); 
        it.printf(id(xp)[i], 98, id(my_font), TextAlign::CENTER_HORIZONTAL, "%s", id(days)[i].c_str()); 
       }

interval:
  - interval: ${update}
    then:
      - if:
          condition:
            lambda: |-
              id(t) += id(it);
              float T = id(my_temp).state;
              float p = id(my_pres).state;
              float h = id(my_hum).state;
              float v = id(my_voc).state;
              if ( (abs(T - id(oldT)) > 0.1f) || (abs(p - id(oldp)) > 0.5f) || (abs(h-id(oldh) > 0.1f) || abs(v-id(oldvoc)) > 2.0f) ){
                id(oldT) = T;
                id(oldp) = p;
                id(oldh) = h;
                id(oldvoc) = v;
                return true;
              } else {
                if( (id(t)%id(ft) == 0) && (id(t) != 0) ) {
                  id(t) = 0;
                  return true;
                } else {
                  return false;
                }
              }
          then:
            - script.execute: my_publish

